Splunk Add-on for Windows 4.8.0
Copyright (C) 2005-2015 Splunk Inc. All Rights Reserved.

Author: Splunk

Source type(s): MonitorWare, NTSyslog, Snare, WinEventLog, DhcpSrvLog, WMI, WindowsUpdateLog, WinRegistry, Perfmon

Input requirements: This add-on supports multiple sources.

Supported product(s): 
* Microsoft DHCP server
* Windows event logs (provided by Splunk, MonitorWare, NTSyslog, or Snare)
* Windows Update log
* Windows Performance metrics
* Windows registry (via the Splunk's change monitoring system)
* Microsoft Internet Authentication Service (IAS)
 
Detailed documentation can be found here - http://docs.splunk.com/Documentation/WindowsAddOn/latest/User/AbouttheSplunkAdd-onforWindows
For Installation and Setup instructions, refer to the section here - http://docs.splunk.com/Documentation/WindowsAddOn/latest/User/InstalltheSplunkAdd-onforWindows
For Release notes, refer to the section here - http://docs.splunk.com/Documentation/WindowsAddOn/latest/User/Releasenotes
