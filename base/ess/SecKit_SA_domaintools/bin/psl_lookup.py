#!/usr/bin/env python

import os
import csv
import sys
import re
import logging, logging.handlers
import splunk

from publicsuffixlist import PublicSuffixList, encode_idn, decode_idn, u, b

def setup_logging():
    logger = logging.getLogger('splunk.foo')    
    SPLUNK_HOME = os.environ['SPLUNK_HOME']
    
    LOGGING_DEFAULT_CONFIG_FILE = os.path.join(SPLUNK_HOME, 'etc', 'log.cfg')
    LOGGING_LOCAL_CONFIG_FILE = os.path.join(SPLUNK_HOME, 'etc', 'log-local.cfg')
    LOGGING_STANZA_NAME = 'python'
    LOGGING_FILE_NAME = "SecKit_domaintools.log"
    BASE_LOG_PATH = os.path.join('var', 'log', 'splunk')
    LOGGING_FORMAT = "%(asctime)s %(levelname)-s\t%(module)s:%(lineno)d - %(message)s"
    splunk_log_handler = logging.handlers.RotatingFileHandler(os.path.join(SPLUNK_HOME, BASE_LOG_PATH, LOGGING_FILE_NAME), mode='a') 
    splunk_log_handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
    logger.addHandler(splunk_log_handler)
    splunk.setupSplunkLogger(logger, LOGGING_DEFAULT_CONFIG_FILE, LOGGING_LOCAL_CONFIG_FILE, LOGGING_STANZA_NAME)
    return logger



""" utiliy the publicsuffic.org and associated algo to parse TLD from domain
"""
# 
def lookup(domain):
    try:
        value = "Y"
        return value
    except:
        return []


def main():
    logger = setup_logging()
    logger.debug("Enter PSL Lookup")
    if len(sys.argv) != 2:
        print "Usage: python external_lookup.py [domain field]"
        sys.exit(1)

    PSLDIR = os.path.join(os.path.dirname(__file__), "../data")
    PSLFILE = os.path.join(PSLDIR, "public_suffix_list.dat")
    pslsource = open(PSLFILE, "rb")
    
    logger.info(PSLFILE)
    psl = PublicSuffixList(source=pslsource)
    domainfield = sys.argv[1]

    infile = sys.stdin
    outfile = sys.stdout

    r = csv.DictReader(infile)
    header = r.fieldnames

    w = csv.DictWriter(outfile, fieldnames=r.fieldnames)
    w.writeheader()

    for result in r:
       result["publicsuffix"] = psl.publicsuffix(result[domainfield], accept_unknown=False)
       result["privatesuffix"] = psl.privatesuffix(result[domainfield], accept_unknown=False)
       #if not result["publicsuffix"]="" and not result["publicsuffix"] and not result["publicsuffix"].isspace():
       if result["publicsuffix"]:
         result["is_domain"]="true"
       else:
         result["is_domain"]="false"
       w.writerow(result)
    pslsource.close()
main()
