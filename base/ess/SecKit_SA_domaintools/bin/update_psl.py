#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2014 ko-zu <causeless@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Enhanced to work within a splunk mod input
import os,sys,re
import logging, logging.handlers
import splunk
from publicsuffixlist import PSLURL, PSLFILE, PublicSuffixList

def setup_logging():
    logger = logging.getLogger('splunk.foo')    
    SPLUNK_HOME = os.environ['SPLUNK_HOME']
    
    LOGGING_DEFAULT_CONFIG_FILE = os.path.join(SPLUNK_HOME, 'etc', 'log.cfg')
    LOGGING_LOCAL_CONFIG_FILE = os.path.join(SPLUNK_HOME, 'etc', 'log-local.cfg')
    LOGGING_STANZA_NAME = 'python'
    LOGGING_FILE_NAME = "SecKit_domaintools.log"
    BASE_LOG_PATH = os.path.join('var', 'log', 'splunk')
    LOGGING_FORMAT = "%(asctime)s %(levelname)-s\t%(module)s:%(lineno)d - %(message)s"
    splunk_log_handler = logging.handlers.RotatingFileHandler(os.path.join(SPLUNK_HOME, BASE_LOG_PATH, LOGGING_FILE_NAME), mode='a') 
    splunk_log_handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
    logger.addHandler(splunk_log_handler)
    splunk.setupSplunkLogger(logger, LOGGING_DEFAULT_CONFIG_FILE, LOGGING_LOCAL_CONFIG_FILE, LOGGING_STANZA_NAME)
    return logger


def updatePSL():
    PSLDIR = os.path.join(os.path.dirname(__file__), "../data")
    PSLFILE = os.path.join(PSLDIR, "public_suffix_list.dat")
   

    try:
        import requests
    except ImportError:
        raise Exception("Please install python-requests http(s) library. $ sudo pip install requests")

    from email.utils import parsedate
    import time

    r = requests.get(PSLURL)
    if r.status_code != requests.codes.ok or len(r.content) == 0:
      logger.error("Could not download PSL from " + PSLURL)  
      raise Exception("Could not download PSL from " + PSLURL)

    lastmod = r.headers.get("last-modified", None)

    f = open(PSLFILE + ".swp", "wb")
    f.write(r.content)
    f.close()

    f = open(PSLFILE + ".swp", "rb")
    psl = PublicSuffixList(f)
    f.close()

    os.rename(PSLFILE + ".swp", PSLFILE)
    
    if lastmod:
        t = time.mktime(parsedate(lastmod))
        os.utime(PSLFILE, (t, t))

    logger.info("PSL updated")
    if lastmod:
        logger.info("PSL FILE last-modified: " + lastmod)

if __name__ == "__main__":
    logger = setup_logging()
    logger.info("Updating PSL")    
    updatePSL()
