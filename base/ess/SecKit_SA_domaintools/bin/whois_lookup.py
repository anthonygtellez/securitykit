#!/usr/bin/env python

import os,datetime
import csv
import sys
import re
import logging, logging.handlers
import splunk

import pythonwhois

def setup_logging():
    logger = logging.getLogger('splunk.foo')    
    SPLUNK_HOME = os.environ['SPLUNK_HOME']
    
    LOGGING_DEFAULT_CONFIG_FILE = os.path.join(SPLUNK_HOME, 'etc', 'log.cfg')
    LOGGING_LOCAL_CONFIG_FILE = os.path.join(SPLUNK_HOME, 'etc', 'log-local.cfg')
    LOGGING_STANZA_NAME = 'python'
    LOGGING_FILE_NAME = "SecKit_domaintools.log"
    BASE_LOG_PATH = os.path.join('var', 'log', 'splunk')
    LOGGING_FORMAT = "%(asctime)s %(levelname)-s\t%(module)s:%(lineno)d - %(message)s"
    splunk_log_handler = logging.handlers.RotatingFileHandler(os.path.join(SPLUNK_HOME, BASE_LOG_PATH, LOGGING_FILE_NAME), mode='a') 
    splunk_log_handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
    logger.addHandler(splunk_log_handler)
    splunk.setupSplunkLogger(logger, LOGGING_DEFAULT_CONFIG_FILE, LOGGING_LOCAL_CONFIG_FILE, LOGGING_STANZA_NAME)
    return logger

""" 
utiliy the whois for a domain
"""
# 
def lookup(domain):
    try:
        value = "Y"
        return value
    except:
        return []


def main():
    logger = setup_logging()
    logger.debug("Enter whois Lookup")
    if len(sys.argv) != 2:
        print "Usage: python external_lookup.py [domain field]"
        sys.exit(1)

	
    domainfield = sys.argv[1]

    infile = sys.stdin
    outfile = sys.stdout

    r = csv.DictReader(infile)
    header = r.fieldnames

    w = csv.DictWriter(outfile, fieldnames=r.fieldnames)
    w.writeheader()

    pipe = "|"
    #domain,id,status,created,expires,updated,nameservers,registrant,regisrar
    for result in r:
	try:
		parsed =pythonwhois.get_whois(result[domainfield],normalized=True)
		try:
			result["id"]         =pipe.join(parsed["id"])
		except:
			pass
		try:	
			result["status"]     =pipe.join(parsed["status"])
		except:
			pass	
		#if result["status"].contains("ctive") :
	#		raise

		try:
			result["created"]    =(parsed["creation_date"][0] - datetime.datetime(1970,1,1)).total_seconds()
		except:
			pass
		try:	
			result["expires"]    =(parsed["expiration_date"][0] - datetime.datetime(1970,1,1)).total_seconds()
		except:
			pass
		try:	
			result["updated"]    =(parsed["updated_date"][0] - datetime.datetime(1970,1,1)).total_seconds()
		except:
			pass
		try:	
			result["nameservers"]=pipe.join(parsed["nameservers"])
		except:
			pass
		try:	
			contacts =parsed["contacts"] 
			registrant = contacts["registrant"]
			result["registrant"] =pipe.join(registrant["name"])
		except:
			pass
		try:	
			result["regisrar"]   =pipe.join(parsed["registrar"])
		except:
			pass
		#if result["created"] :
		w.writerow(result)
	except:
		pass
main()
