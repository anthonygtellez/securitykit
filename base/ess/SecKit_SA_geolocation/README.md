#Welcome
This supporting add-on (SA) for Splunk advanced IP information enrichment using the MaxMind family of databases. 

https://www.maxmind.com/en/geoip2-databases
Supported 
* 	City2Lite
* 	City2 (optional)
* 	ISP  (optional)
* 	ConnectionType (optional)
	
#Installation
* Install the SecKit_SA_geolocation on each searc head
* From the SSH Shell execute $SPLUNK_HOME/etc/apps/SecKit_SA_geolocation/bin/SecKit_geo_update.sh 

#USAGE
* | `seckit_iplocation(fieldname)`
* | `seckit_iplocation(fieldname,prefix)`

Where fieldname is the name of the field containing the IP
prefix is the prefix to assign to the output fields
#Example
| `seckit_iplocation(src,geo)`
#FIELDS
* city (City/CityLite)
* country (City/CityLite)
* lat (City/CityLite)
* long (City/CityLite)
* connection_type (ConnectionType)
* isp (ISP)
* isp_organization (ISP) 
* isp_ip (ISP)
* isp_asn (ISP)
* isp_asn_organization (ISP)

#Source
https://bitbucket.org/rfaircloth-splunk/securitykit/src/53c4cf4e659676ead6a3aaeb54be207278f642d3/base/ess/SecKit_SA_geolocation/?at=master

#Legal
* *Splunk* is a registered trademark of Splunk, Inc.
* *MaxMind* is a registered trademark of MaxMind, Inc.