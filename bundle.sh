
#package
COPYFILE_DISABLE=1 tar -czf ./spl/SecKit_SA_idm_common.spl --exclude='.*' --exclude="._*" -C base/ess SecKit_SA_idm_common
COPYFILE_DISABLE=1 tar -czf ./spl/SecKit_SA_idm_windows.spl --exclude='.*' --exclude="._*" -C base/ess SecKit_SA_idm_windows
COPYFILE_DISABLE=1 tar -czf ./spl/SecKit_SA_geolocations.spl --exclude='.*' --exclude="._*" -C base/ess SecKit_SA_geolocation

COPYFILE_DISABLE=1 tar -czf ./spl/SA-SecKit-EndpointProtection.spl --exclude='.*' --exclude="._*" -C base/ess SA-SecKit-EndpointProtection
COPYFILE_DISABLE=1 tar -czf ./spl/DA-ESS-SecKit-AccessProtection.spl --exclude='.*' --exclude="._*" -C base/ess DA-ESS-SecKit-AccessProtection
COPYFILE_DISABLE=1 tar -czf ./spl/DA-ESS-SecKit-EndpointProtection.spl --exclude='.*' --exclude="._*" -C base/ess DA-ESS-SecKit-EndpointProtection
COPYFILE_DISABLE=1 tar -czf ./spl/DA-ESS-SecKit-NetworkProtection.spl --exclude='.*' --exclude="._*" -C base/ess DA-ESS-SecKit-NetworkProtection
#
